# Covid-19 Tracker
[![License: GPL v2](https://img.shields.io/badge/License-GPL%20v2-blue.svg)](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html) [![Open Source Love](https://badges.frapsoft.com/os/v1/open-source.svg?v=102)](https://opensource.org/licenses/MIT) 

COVID-19 is the infectious disease caused by the most recently discovered coronavirus. This new virus and disease were unknown before the outbreak began in Wuhan, China, in December 2019. (WHO)

[COVID-19 Details](https://www.who.int/news-room/q-a-detail/q-a-coronaviruses)

This app was made for training. For arrangements and development ideas, please contact me.

# UI Design
Made with **Adobe XD**.

![UI](https://github.com/mukireus/flutter_covid_19_tracker/blob/master/assets/ui.gif)




